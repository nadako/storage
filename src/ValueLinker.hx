@:access(Value)
class ValueLinker<T:Value> implements Linker<T> {
	public inline function new() {}

	public inline function link(value:T, parent:Value, name:String) {
		#if (cs || java) var value:Value = value; #end // see https://github.com/HaxeFoundation/haxe/issues/6385
		value.__link(parent, name);
	}

	public inline function unlink(value:T) {
		#if (cs || java) var value:Value = value; #end // see https://github.com/HaxeFoundation/haxe/issues/6385
		value.__unlink();
	}
}
