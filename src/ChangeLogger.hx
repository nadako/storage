class ChangeLogger {
	var changes:Array<Change>;

	public function new() {
		changes = [];
	}

	public function logChange(path:Array<String>, value:Object) {
		trace('Logging change: $path -> ${haxe.Json.stringify(value)}');
		changes.push({path: path, value: value});
	}
}
