import haxe.Json;

enum Skill {
	EInvis;
	EDamage(percent:Float);
	ERecursive(o:Effect);
}

enum Effect {
	Some;
}

class Player extends Value {
	public var name:String;
	public var level:Int;
	public var skill:Skill;

	public function new() {}
}

class Data extends Value {
	public var player:Player;
	public var score:Int;

	public function new() {}

	public inline function setChangeLogger(logger) {
		__setLogger(logger);
	}

	public function toObject() {
		return __getConverter().toObject(this);
	}
}

class Main {
	static function main() {
		var data = new Data();
		data.setChangeLogger(new ChangeLogger());
		data.score = 10;

		var p = new Player();
		p.name = "Dan";
		p.level = 42;

		data.player = p;
		p.level++;

		data.player = null;
		p.level++;
		data.player = p;
		p.skill = ERecursive(Some);

		trace("Serialized: " + Json.stringify(data.toObject()));
	}
}
