#if macro
import haxe.macro.Context;
import haxe.macro.Expr;
import haxe.macro.Type;
using haxe.macro.Tools;

typedef GetHelpersResult = {
	var fieldToObjectExpr:Expr;
	var newValueToObjectExpr:Expr;
	var linkingExpr:Expr;
	var setLoggerExpr:Null<Expr>;
}

class ValueMacro {
	static function build() {
		var fields = Context.getBuildFields();
		var newFields = new Array<Field>();

		var pos:Position;
		var thisModule:String;
		var thisTP:TypePath;
		switch Context.getLocalType() {
			case TInst(_.get() => cl, params):
				pos = cl.pos;
				thisModule = cl.module;
				thisTP = getTypePath(cl);
				if (params.length > 0) {
					// this will be implemented, but not right now :)
					throw new Error("Value subclasses with type parameters are not supported yet", pos);
				}
			case _:
				throw "impossiburu"; // can't happen: it's surely a TInst when we're building a class
		}

		var converterToObjectDeclFields = new Array<{field:String, expr:Expr}>();
		var setLoggerExprs = new Array<Expr>();

		for (field in fields) {
			if (field.access.indexOf(AStatic) != -1)
				continue; // skip static fields

			switch (field.kind) {
				case FFun(_) | FProp("get" | "never", "set" | "never", _, _):
					// skip methods and non-physical properties

				case FProp(_, _, _, _):
					throw new Error("Physical properties on Value subclasses are not supported", field.pos);

				case FVar(type, expr):
					if (expr != null) throw new Error("Fields of Value subclasses cannot have initializer expressions", expr.pos);
					if (type == null) throw new Error("Fields of Value subclasses must have explicit type hint", field.pos);
					var fieldName = field.name; // store in a local var for easier reification

					// make it a property with a setter
					field.kind = FProp("default", "set", type, null);

					var helpers = getHelpers(type.toType(), field.pos, macro this.$fieldName, macro value, macro oldValue,  macro this, macro $v{fieldName}, macro value.$fieldName);

					if (helpers.setLoggerExpr != null)
						setLoggerExprs.push(helpers.setLoggerExpr);

					converterToObjectDeclFields.push({field: fieldName, expr: helpers.fieldToObjectExpr});

					var setterExpr = macro {
						var oldValue = this.$fieldName;
						if (oldValue != value) {
							${helpers.linkingExpr};
							this.$fieldName = value;
							if (__changeLogger != null)
								__changeLogger.logChange(__getFieldPath($v{fieldName}), ${helpers.newValueToObjectExpr});
						}
						return value;
					};

					newFields.push({
						pos: field.pos,
						name: 'set_$fieldName',
						kind: FFun({
							args: [{name: "value", type: type}],
							ret: type,
							expr: setterExpr
						})
					});
			}
		}

		var thisCT = TPath(thisTP);
		{
			var converterClassName = '${thisTP.sub}__Converter';
			var toObjectDecl = {pos: pos, expr: EObjectDecl(converterToObjectDeclFields)};
			var toObjectExpr = macro return $toObjectDecl;
			var converterTP:TypePath = {pack: thisTP.pack, name:converterClassName};
			Context.defineType({
				pos: pos,
				pack: thisTP.pack,
				name: converterClassName,
				kind: TDClass(null, [{pack: [], name: "Converter", params: [TPType(thisCT)]}], false),
				fields: [
					{
						pos: pos,
						name: "new",
						access: [APublic, AInline],
						kind: FFun({args: [], ret: null, expr: macro {}})
					},
					{
						pos: pos,
						name: "toObject",
						access: [APublic, AInline],
						kind: FFun({args: [{name: "value", type: thisCT}], ret: macro : Object, expr: toObjectExpr})
					},
				],
			}, thisModule);

			newFields.push({
				pos: pos,
				name: "__converter",
				access: [AStatic],
				kind: FVar(null, macro new $converterTP())
			});

			newFields.push({
				pos: pos,
				name: "__getConverter",
				access: [AStatic, AInline],
				kind: FFun({
					args: [],
					ret: null,
					expr: macro return __converter
				})
			});
		}

		{
			newFields.push({
				pos: pos,
				name: "__linker",
				access: [AStatic],
				kind: FVar(null, macro new ValueLinker<$thisCT>())
			});

			newFields.push({
				pos: pos,
				name: "__getLinker",
				access: [AStatic, AInline],
				kind: FFun({
					args: [],
					ret: null,
					expr: macro return __linker
				})
			});
		}

		if (setLoggerExprs.length > 0) {
			setLoggerExprs.unshift(macro super.__setLogger(logger));
			newFields.push({
				pos: pos,
				name: "__setLogger",
				access: [AOverride],
				kind: FFun({
					args: [{name: "logger", type: null}],
					ret: null,
					expr: macro $b{setLoggerExprs}
				})
			});
		}

		return fields.concat(newFields);
	}

	static function isValueSubclass(cl:ClassType) {
		return switch cl {
			case {pack: [], name: "Value"}: true;
			case _ if (cl.superClass != null): isValueSubclass(cl.superClass.t.get());
			case _: false;
		}
	}

	static function getHelpers(type:Type, pos:Position, fieldExpr:Expr, newValueExpr:Expr, oldValueExpr:Expr, parentExpr:Expr, nameExpr:Expr, converterValueFieldExpr:Expr):GetHelpersResult {
		inline function basic() return {
			fieldToObjectExpr: converterValueFieldExpr,
			newValueToObjectExpr: newValueExpr,
			linkingExpr: macro {},
			setLoggerExpr: null,
		};

		switch type {
			case TInst(_.get() => cl, params):
				switch [cl, params] {
					case [{pack: [], name: "String"}, _]:
						return basic();

					case _ if (isValueSubclass(cl)):
						var tp = getTypePath(cl);
						var typePathExpr = macro $p{tp.pack.concat([tp.name, tp.sub])};
						return {
							fieldToObjectExpr: macro if ($converterValueFieldExpr != null) @:privateAccess $typePathExpr.__getConverter().toObject($converterValueFieldExpr) else null,
							newValueToObjectExpr: macro if ($newValueExpr != null) @:privateAccess $typePathExpr.__getConverter().toObject($newValueExpr) else null,
							linkingExpr: macro {
								var linker = @:privateAccess $typePathExpr.__getLinker();
								if ($oldValueExpr != null) linker.unlink($oldValueExpr);
								if ($newValueExpr != null) linker.link($newValueExpr, $parentExpr, $nameExpr);
							},
							setLoggerExpr: macro if ($fieldExpr != null) $fieldExpr.__setLogger(logger),
						};

					case _:
				}

			case TAbstract(_.get() => ab, params):
				switch [ab, params] {
					case [{pack: [], name: "Int" | "Float" | "Bool"}, _]:
						return basic();

					case _:
				}

			case TEnum(_.get() => en, params):
				if (params.length > 0)
					throw new Error("Value subclasses with type parameters are not supported yet", pos);

				var converterCases = new Array<Case>();

				for (ctorName in en.names) {
					var field = en.constructs[ctorName];
					var ctorIdent = macro $i{ctorName};
					switch field.type {
						case TEnum(_, _):
							converterCases.push({
								values: [ctorIdent],
								expr: macro {$ctorName: true}
							});
						case TFun(args, _):
							var argIdents = [];
							var valueObjectDeclFields = [];
							for (arg in args) {
								var argIdent = macro $i{arg.name};
								argIdents.push(argIdent);
								var helpers = getHelpers(arg.t, field.pos, null, null, null, null, null, argIdent);
								valueObjectDeclFields.push({
									field: arg.name,
									expr: helpers.fieldToObjectExpr,
								});
							}
							var valueObjectDecl = {pos: pos, expr: EObjectDecl(valueObjectDeclFields)};
							converterCases.push({
								values: [macro $ctorIdent($a{argIdents})],
								expr: macro {$ctorName: $valueObjectDecl}
							});
						case _: throw "impossiburu"; // enum fields are always either values or constructor functions
					}
				}

				var thisTP = getTypePath(en);

				var thisCT = TPath(thisTP);
				var converterClassName = '${thisTP.sub}__Converter';
				var switchExpr = {pos: pos, expr: ESwitch(macro value, converterCases, null)};
				var converterTP:TypePath = {pack: thisTP.pack, name: converterClassName};
				Context.defineType({
					pos: pos,
					pack: thisTP.pack,
					name: converterClassName,
					kind: TDClass(null, [{pack: [], name: "Converter", params: [TPType(thisCT)]}], false),
					fields: [
						{
							pos: pos,
							name: "new",
							access: [APublic, AInline],
							kind: FFun({args: [], ret: null, expr: macro {}})
						},
						{
							pos: pos,
							name: "toObject",
							access: [APublic],
							kind: FFun({args: [{name: "value", type: thisCT}], ret: macro : Object, expr: macro return $switchExpr})
						},
						{
							pos: pos,
							name: "instance",
							access: [AStatic],
							kind: FVar(null, macro new $converterTP())
						},
						{
							pos: pos,
							name: "get",
							access: [APublic, AInline, AStatic],
							kind: FFun({args: [], ret: null, expr: macro return instance})
						},
					],
				}, en.module);

				var converterTypePathExpr = macro $p{thisTP.pack.concat([converterClassName])};
				return {
					fieldToObjectExpr: macro if ($converterValueFieldExpr != null) $converterTypePathExpr.get().toObject($converterValueFieldExpr) else null,
					newValueToObjectExpr: macro if ($newValueExpr != null) $converterTypePathExpr.get().toObject($newValueExpr) else null,
					linkingExpr: macro {},
					setLoggerExpr: null,
				};

			case _:
		}
		throw new Error("Unsupported type for Value subclass fields: " + type.toString(), pos);
	}

	static function getTypePath(t:BaseType):TypePath {
		var moduleName = t.module.split(".").pop();
		return {pack: t.pack, name: moduleName, sub: t.name, params: []}; // TODO: params
	}
}
#end
