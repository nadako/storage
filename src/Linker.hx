interface Linker<T> {
	function link(value:T, parent:Value, name:String):Void;
	function unlink(value:T):Void;
}
