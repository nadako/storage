@:autoBuild(ValueMacro.build())
class Value {
	var __parent:Value;
	var __name:String;
	var __changeLogger:ChangeLogger;

	function __link(parent, name) {
		if (__parent != null) throw "Object is already linked";
		__parent = parent;
		__name = name;
		__setLogger(__parent.__changeLogger);
	}

	function __setLogger(logger) {
		__changeLogger = logger;
	}

	function __unlink() {
		__parent = null;
		__name = null;
		__setLogger(null);
	}

	function __getFieldPath(field:String):Array<String> {
		var path = [field];
		var value = this;
		while (value.__parent != null) {
			path.push(value.__name);
			value = value.__parent;
		}
		path.reverse();
		return path;
	}
}
